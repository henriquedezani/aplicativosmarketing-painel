import firebase_admin
from firebase_admin import credentials, firestore

# Use the application default credentials
cred = credentials.Certificate('feiraappmkt20181-firebase-adminsdk-0l92l-6e161d29e7.json')
firebase_admin.initialize_app(cred)

db = firestore.client()

data = [
    {'id': 'teacherstool', 'nome': 'Teachers''Tool', 'curso': 'Informática para Negócios', 'tipo': 'aplicativo', 'periodo': 'Manhã',
     'logo': 'https://firebasestorage.googleapis.com/v0/b/feiraappmkt20181.appspot.com/o/logo%2FScreen%20Shot%202018-06-06%20at%2018.40.04.png?alt=media&token=83173382-2582-42f1-8275-f06e662cb7c5', 'url': 'https://play.google.com/store/apps/details?id=br.com.ll.teacherstool'},

    {'id': 'compare', 'nome': 'Compare!', 'curso': 'Informática para Negócios', 'tipo': 'aplicativo', 'periodo': 'Manhã',
        'logo': 'https://firebasestorage.googleapis.com/v0/b/feiraappmkt20181.appspot.com/o/logo%2FScreen%20Shot%202018-06-06%20at%2018.40.14.png?alt=media&token=c80dbe52-8459-4bf4-bad3-b4cb4472026e', 'url': 'https://play.google.com/store/apps/details?id=br.edu.fatecriopreto.feiraplicativomarketing.compare'},

    {'id': 'memoplay', 'nome': 'memoPlay', 'curso': 'Informática para Negócios', 'tipo': 'aplicativo', 'periodo': 'Manhã',
     'logo': 'https://firebasestorage.googleapis.com/v0/b/feiraappmkt20181.appspot.com/o/logo%2FScreen%20Shot%202018-06-06%20at%2018.39.58.png?alt=media&token=5a470b8f-f780-4d6c-bc3b-653eded70eb4', 'url': 'https://play.google.com/store/apps/details?id=br.edu.fatecriopreto.feiraaplicativomarketing.memoplay'},

    {'id': 'deleiteapp', 'nome': 'DeLeiteApp', 'curso': 'Informática para Negócios', 'tipo': 'aplicativo', 'periodo': 'Noite',
     'logo': 'https://firebasestorage.googleapis.com/v0/b/feiraappmkt20181.appspot.com/o/logo%2FScreen%20Shot%202018-06-06%20at%2018.40.32.png?alt=media&token=dc7e43f4-bf4e-4d8f-b5b1-b7b7356400f9', 'url': 'https://play.google.com/store/apps/details?id=br.edu.fatecriopreto.feiraaplicativomarketing.deleiteapp'},

    {'id': 'acheinoapp', 'nome': 'Achei no App', 'curso': 'Informática para Negócios', 'tipo': 'aplicativo', 'periodo': 'Noite',
     'logo': 'https://firebasestorage.googleapis.com/v0/b/feiraappmkt20181.appspot.com/o/logo%2FScreen%20Shot%202018-06-06%20at%2018.40.39.png?alt=media&token=034af568-a103-4abf-978c-6da4a586ac4c', 'url': 'https://play.google.com/store/apps/details?id=br.edu.fatecriopreto.feiraaplicativomarketing.acheinoapp'},

    {'id': 'rewardgo', 'nome': 'RewardGO', 'curso': 'Informática para Negócios', 'tipo': 'aplicativo', 'periodo': 'Noite',
     'logo': 'https://firebasestorage.googleapis.com/v0/b/feiraappmkt20181.appspot.com/o/logo%2FScreen%20Shot%202018-06-06%20at%2018.40.48.png?alt=media&token=24239d53-6d7c-445c-b727-ec17795fbb3b', 'url': 'https://play.google.com/store/apps/details?id=br.edu.fatecriopreto.feiraaplicativomarketing.rewardgo'},

    {'id': 'mobboy', 'nome': 'mobBoy', 'curso': 'Informática para Negócios', 'tipo': 'aplicativo', 'periodo': 'Noite',
     'logo': 'https://firebasestorage.googleapis.com/v0/b/feiraappmkt20181.appspot.com/o/logo%2FScreen%20Shot%202018-06-06%20at%2018.39.34.png?alt=media&token=8282841e-9aac-4806-8f1d-2dbe224256d3', 'url': 'https://play.google.com/store/apps/details?id=br.edu.fatecriopreto.feiraaplicativomarketing.mobboy'},

    {'id': 'nearbeer', 'nome': 'Near Beer', 'curso': 'Informática para Negócios', 'tipo': 'aplicativo', 'periodo': 'Noite',
     'logo': 'https://firebasestorage.googleapis.com/v0/b/feiraappmkt20181.appspot.com/o/logo%2FScreen%20Shot%202018-06-06%20at%2018.39.50.png?alt=media&token=85018a9c-5feb-4995-80dd-de0a904f08da', 'url': 'https://play.google.com/store/apps/details?id=br.edu.fatecriopreto.feiraaplicativomarketing.nearbeer'},

    {'id': 'ticketme', 'nome': 'TicketMe', 'curso': 'Análise e Desenvolvimento de Sistemas', 'tipo': 'aplicativo', 'periodo': 'Tarde',
     'logo': 'https://firebasestorage.googleapis.com/v0/b/feiraappmkt20181.appspot.com/o/logo%2FScreen%20Shot%202018-06-06%20at%2018.40.43.png?alt=media&token=0e31edaa-4bf7-45af-9270-08d3be9cc95b', 'url': 'https://play.google.com/store/apps/details?id=br.edu.fatecriopreto.feiraaplicativomarketing.ticketme'},

    {'id': 'icarne', 'nome': 'iCarne', 'curso': 'Análise e Desenvolvimento de Sistemas', 'tipo': 'aplicativo', 'periodo': 'Tarde',
     'logo': 'https://firebasestorage.googleapis.com/v0/b/feiraappmkt20181.appspot.com/o/logo%2FScreen%20Shot%202018-06-06%20at%2018.40.58.png?alt=media&token=9253c1e4-8a5a-497d-849d-cb94ff5a066f', 'url': 'https://play.google.com/store/apps/details?id=br.edu.fatecriopreto.feiraaplicativomarketing.icarne'},

    {'id': 'descartelegal', 'nome': 'Descarte Legal', 'curso': 'Análise e Desenvolvimento de Sistemas', 'tipo': 'aplicativo', 'periodo': 'Tarde',
     'logo': 'https://firebasestorage.googleapis.com/v0/b/feiraappmkt20181.appspot.com/o/logo%2FScreen%20Shot%202018-06-06%20at%2018.41.04.png?alt=media&token=f5de5844-cefc-4d40-80e8-cd923cb6e8ad', 'url': 'https://play.google.com/store/apps/details?id=br.edu.fatecriopreto.feiraaplicativomarketing.descartelegal'},

    {'id': 'pars', 'nome': 'ParS', 'curso': 'Análise e Desenvolvimento de Sistemas', 'tipo': 'aplicativo', 'periodo': 'Tarde',
     'logo': 'https://firebasestorage.googleapis.com/v0/b/feiraappmkt20181.appspot.com/o/logo%2FScreen%20Shot%202018-06-06%20at%2018.41.08.png?alt=media&token=751ba1c3-d5e6-482b-83e8-a89b5da2c967', 'url': 'https://play.google.com/store/apps/details?id=br.edu.fatecriopreto.feiraaplicativomarketing.pars'},

    {'id': 'riopretoalerta', 'nome': 'Rio Preto Alerta', 'curso': 'Análise e Desenvolvimento de Sistemas', 'tipo': 'aplicativo', 'periodo': 'Tarde',
     'logo': 'https://firebasestorage.googleapis.com/v0/b/feiraappmkt20181.appspot.com/o/logo%2FScreen%20Shot%202018-06-06%20at%2018.41.13.png?alt=media&token=114f0db4-7e89-4a95-bf47-b2111564b71e', 'url': 'https://play.google.com/store/apps/details?id=br.edu.fatecriopreto.feiraaplicativomarketing.riopretoalerta'},

    {'id': 'biocontrol', 'nome': 'Bio Control', 'curso': 'Agronegócio', 'tipo': 'agronegocio', 'periodo': 'Noite', 'logo': 'https://firebasestorage.googleapis.com/v0/b/feiraappmkt20181.appspot.com/o/logo%2FScreen%20Shot%202018-06-06%20at%2020.17.29.png?alt=media&token=2827447e-7c49-476e-b734-4e30dda27a9b'},

    {'id': 'quesaudavel', 'nome': 'Que Saudável', 'curso': 'Agronegócio', 'tipo': 'agronegocio', 'periodo': 'Manhã', 'logo': 'https://firebasestorage.googleapis.com/v0/b/feiraappmkt20181.appspot.com/o/logo%2FScreen%20Shot%202018-06-06%20at%2020.18.54.png?alt=media&token=b7b31f3b-f763-4fdf-ba26-0562d165a2cc'},

    {'id': 'myfit', 'nome': 'My Fit', 'curso': 'Agronegócio', 'tipo': 'agronegocio', 'periodo': 'Manhã', 'logo': 'https://firebasestorage.googleapis.com/v0/b/feiraappmkt20181.appspot.com/o/logo%2FScreen%20Shot%202018-06-06%20at%2020.19.02.png?alt=media&token=ecb1dcbe-37e8-4080-aa0c-082bbe7032f5'},

    {'id': 'greenplast', 'nome': 'GreenPlast', 'curso': 'Agronegócio', 'tipo': 'agronegocio', 'periodo': 'Noite', 'logo': 'https://firebasestorage.googleapis.com/v0/b/feiraappmkt20181.appspot.com/o/logo%2FScreen%20Shot%202018-06-06%20at%2020.19.11.png?alt=media&token=1d05e96d-843c-459a-aa11-05701e2e69e4'},

    {'id': 'viversaudavel', 'nome': 'Viver Saudável', 'curso': 'Agronegócio', 'tipo': 'agronegocio', 'periodo': 'Noite', 'logo': 'https://firebasestorage.googleapis.com/v0/b/feiraappmkt20181.appspot.com/o/logo%2FScreen%20Shot%202018-06-06%20at%2020.19.21.png?alt=media&token=fec6b3e5-82be-4217-a7ca-789f927e2053'},

    {'id': 'ruralconnect', 'nome': 'Rural Connect', 'curso': 'Agronegócio', 'tipo': 'agronegocio', 'periodo': 'Manhã', 'logo': 'https://firebasestorage.googleapis.com/v0/b/feiraappmkt20181.appspot.com/o/logo%2FScreen%20Shot%202018-06-06%20at%2020.19.35.png?alt=media&token=6e68bfdc-c380-4091-8220-f7bcdef38e01'},

    {'id': 'icefitness', 'nome': 'Ice Fitness', 'curso': 'Agronegócio', 'tipo': 'agronegocio', 'periodo': 'Manhã', 'logo': 'https://firebasestorage.googleapis.com/v0/b/feiraappmkt20181.appspot.com/o/logo%2FScreen%20Shot%202018-06-06%20at%2020.19.29.png?alt=media&token=6e616802-3716-44d1-a484-ce27e19d36c6'},

    {'id': 'aesfirra', 'nome': 'A Esfirra', 'curso': 'Agronegócio', 'tipo': 'agronegocio', 'periodo': 'Noite', 'logo': 'https://firebasestorage.googleapis.com/v0/b/feiraappmkt20181.appspot.com/o/logo%2FScreen%20Shot%202018-06-06%20at%2020.21.49.png?alt=media&token=f65fee8a-bda9-4c00-b1b1-aa72f74001c5'},

    {'id': 'deleiteagro', 'nome': 'De Leite App', 'curso': 'Agronegócio', 'tipo': 'agronegocio', 'periodo': 'Noite', 'logo': 'https://firebasestorage.googleapis.com/v0/b/feiraappmkt20181.appspot.com/o/logo%2FScreen%20Shot%202018-06-06%20at%2018.40.32.png?alt=media&token=dc7e43f4-bf4e-4d8f-b5b1-b7b7356400f9'},

]

for d in data:
    db.collection('projetos').document(d['id']).update(d, firestore.CreateIfMissingOption(True))